<?php

namespace DICIT\Tools\Validation;

use DICIT\ArrayResolver;

interface ConfigValidator
{

    /**
     * @return void
     */
    public function validateService(Validator $validator, ArrayResolver $global, $serviceName, ArrayResolver $serviceNode);
}
