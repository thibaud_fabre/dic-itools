<?php

namespace DICIT\Tools\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LogLevel;
use DICIT\Config\ConfigFactory;

class ListParametersCommand extends BaseCommand
{

    protected function configure()
    {
        $this->setName('list-params')
            ->setDescription('List all parameters')
            ->addArgument('file', InputArgument::REQUIRED, 'The main DIC-IT configuration file');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');

        $logger = $this->buildLogger($output);
        $config = ConfigFactory::fromFile($file);

        $config->load();

        $this->dumpParameters($logger, $file, $config);
    }

    protected function dumpParameters($logger, $file, $config)
    {
        $logger->info('Defined parameters : ' . $file);
        $data = $config->getData();

        foreach ($data['parameters'] as $name => $value) {
            $logger->info($name);
        }

        $logger->info('Defined parameters : ' . $file);
        $logger->info('Not implemented yet');
    }
}
