<?php

namespace DICIT\Tools\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DICIT\Tools\Validation\Validator;
use DICIT\Tools\Validation\DependencyValidator;
use DICIT\Tools\Validation\ConstructorArgumentsValidator;
use DICIT\Tools\Validation\EmptyNodeValidator;
use Psr\Log\LogLevel;
use DICIT\Tools\Validation\CyclicDependencyValidator;
use DICIT\Config\ConfigFactory;

class ConfigCheckCommand extends BaseCommand
{

    protected function configure()
    {
        $this->setName('check')
            ->setDescription('Validate a DIC-IT configuration file')
            ->addArgument('file', InputArgument::REQUIRED, 'The main DIC-IT configuration file')
            ->addOption('ignore', null, InputOption::VALUE_OPTIONAL, 'List of ignored service names.', '');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');
        $ignores = explode(',', $input->getOption('ignore'));

        $logger = $this->buildLogger($output, LogLevel::WARNING);
        $config = ConfigFactory::fromFile($file);

        $output->writeln(PHP_EOL . 'Parsing YAML configuration file : ' . $file . PHP_EOL);

        $validator = $this->buildValidator($config, $logger, $ignores);
        $validator->validate();

        $output->writeln(PHP_EOL . 'Done.');
    }

    private function buildValidator($config, $logger, $ignores)
    {
        $validator = new Validator($config, $logger);
        $validator->onNodeInspected(function () use($logger)
        {
            $logger->resetStack();
        });

        $validator->add(new EmptyNodeValidator());

        $dependencyValidator = new DependencyValidator();
        foreach ($ignores as $ignore) {
            $dependencyValidator->ignore($ignore);
        }

        $validator->add($dependencyValidator);
        $validator->add(new ConstructorArgumentsValidator());
        $validator->add(new CyclicDependencyValidator());

        return $validator;
    }
}
