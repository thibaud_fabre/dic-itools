<?php

namespace DICIT\Tools\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends Command
{

    protected function buildLogger(OutputInterface $output, $threshold = null)
    {
        $logger = new CommandLogger($output);

        if ($threshold != null) {
            $logger->enableThreshold(LogLevel::WARNING);
        }

        $logger->enableFiltering();
        $logger->addLevel(LogLevel::ERROR);
        $logger->addLevel(LogLevel::WARNING);
        $logger->addLevel(LogLevel::INFO);

        return $logger;
    }
}
