dic-itools
==========

[![Build Status](https://travis-ci.org/aztech-dev/dic-itools.svg?branch=master)](https://travis-ci.org/aztech-dev/dic-itools)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/aztech-dev/dic-itools/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/aztech-dev/dic-itools/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/aztech-dev/dic-itools/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/aztech-dev/dic-itools/?branch=master)
[![Dependency Status](https://www.versioneye.com/user/projects/53b92a87609ff0a361000002/badge.svg)](https://www.versioneye.com/user/projects/53b92a87609ff0a361000002)
[![HHVM Status](http://hhvm.h4cc.de/badge/evaneos/dic-itools.png)](http://hhvm.h4cc.de/package/evaneos/dic-itools)

Tools for the DIC-IT library
 
